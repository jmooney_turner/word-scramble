// LetterIncrement.js
function LetterIncrement(startingIndex, availableCharacters)
{
	this.availableCharacters = availableCharacters;
	this.letterIndex = startingIndex;
	this.sprite = null;
	
	this.init = function(sprite)
	{
		this.sprite = sprite;
        this.sprite.animation.text = this.getCharacter();
	}
	
	this.getCharacter = function()
	{
		return this.availableCharacters[this.letterIndex];	
	}
	
   this.onClick = function(coord)
   {
      this.letterIndex++;
      this.letterIndex = this.letterIndex % this.availableCharacters.length;
      this.sprite.animation.text = this.getCharacter();
   }
}
