// WordHandler.js
function WordHandler(target)
{
	this.target = target;
	this.sprite = null;
	this.letters = [];
	
	this.init = function(sprite)
	{
		this.sprite = sprite;
	}	
	
	this.update = function(elapsed)
	{
		var word = "";
		for (var i = 0; i < this.letters.length; i++)
		{
			word += this.letters[i].getCharacter();
		}
		this.sprite.animation.text = word == this.target ? "YOU WIN!" : "GUESS THE WORD";
	}
}