// GUICamera.js

function GUICamera(camera)
{
	this.camera = camera;
	
	
	this.paint = function(image, srcRect, destRect)
	{
		//console.log(srcRect + "; " + destRect);
		this.camera.context.drawImage(image, srcRect.left, srcRect.top, srcRect.width, srcRect.height,
            destRect.left, destRect.top, destRect.width, destRect.height); 
	}
}
