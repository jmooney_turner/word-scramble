// Point.js

Point = function(x, y)
{
	this.x = x;
	this.y = y;
	
	
}

Point.prototype.toString = function()
{
	return "[x=" + this.x + "; y=" + this.y + "]";
}

Point.distance = function(pnt1, pnt2)
{
	return Math.sqrt(Point.distance(pnt1, pnt2));
}

Point.distanceSqred = function(pnt1, pnt2)
{
	var Asqred = pnt1.x - pnt2.x;
	var Bsqred = pnt1.y - pnt2.y;
	Asqred *= Asqred;
	Bsqred *= Bsqred;
	
	return Asqred + Bsqred;
}

Point.angle = function(pnt1, pnt2)
{
	var xOffset = pnt2.x - pnt1.x;
	var yOffset = pnt2.y - pnt1.y; 
	var theta = Math.atan2(Math.abs(yOffset), Math.abs(xOffset));
	if (xOffset < 0 && yOffset >= 0)
	{
		theta = Math.PI - theta;
	}
	else if (xOffset < 0)
	{
		theta = Math.PI + theta;
	}
	else if (yOffset <= 0)
	{
		theta = -theta;
	}
	
    while(theta < 0)
	{
		theta += 2 * Math.PI;
	}
	
	return theta;
}
