// RectangleDisplay.js

function RectangleDisplay(color)
{
      this.color = color;
      
     this.paint = function(camera, dest)
     {
        camera.paintRect(dest, this.color);
     }
}