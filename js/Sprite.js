// Sprite.js

function Sprite(anim)
{
   this.animation = anim;
   this.position = {x : 0, y : 0};
   this.size = {width : 0, height : 0};
   this.components = [];
   this.isGUI = false;
   
   this.moveX = function(val)
   {
      this.position.x += val;
   }

   this.moveY = function(val)
   {
      this.position.y += val;
   }

   this.setPosition = function(x, y)
   {
      this.position.x = x;
      this.position.y = y;
   }
  
   this.setDimensions = function(width, height) 
   {
   	  this.size.width = width;
   	  this.size.height = height;
   }
   
   this.setDimension = this.setDimensions;
   
   this.getBounds = function()
   {
   	  return new Rectangle(this.position.x, this.position.y, this.size.width, this.size.height);
   }
   
   this.addComponent = function(component)
   {
   	  this.components.push(component);
   	  if (component.init)
   	  {
   	  	component.init(this);
   	  }
   }
   
   this.update = function(elapsed)
   {
   	  if (this.animation.update)
   	  {
   	  	this.animation.update(elapsed);
   	  }
   	  for (var i = 0; i < this.components.length; i++)
   	  {
   	  	var component = this.components[i];
   	  	if(component.update)
   	  	{
   	    	component.update(elapsed);
   	    }
   	  }
   	  for (var i = 0; i < this.components.length; i++)
   	  {
   	  	var component = this.components[i];
   	  	if(component.lateUpdate)
   	  	{
   	    	component.lateUpdate(elapsed);
   	    }
   	  }
   }
   
   this.paint = function(camera)
   {
   	
   	 gcamera = new GUICamera(camera);
   	 if(this.isGUI)
   	 {
   	  	 this.animation.paint(gcamera, new Rectangle(this.position.x, this.position.y, this.size.width, this.size.height));
   	 }
   	 else
   	 {
   	 	this.animation.paint(camera, new Rectangle(this.position.x, this.position.y, this.size.width, this.size.height));
   	 }
   }
   
   this.setAnimation = function(anim)
   {
   	  if(this.animation == anim)
   	  {
   	  	  return;
   	  }

  	  if(anim.reset)
  	  {
  	  	  anim.reset();
  	  }
	  this.animation = anim;
   }
   
   this.processClick = function(worldCoord, viewCoord)
   {
   	  var coord = this.isGUI ? viewCoord : worldCoord;
   	  if (!this.containsCoord(coord))
   	  {
   	  	return false;
   	  }
   	  for (var i = 0; i < this.components.length; i++)
   	  {
   	  	var component = this.components[i];
   	  	if(component.onClick)
   	  	{
   	    	component.onClick(coord);
   	    }
   	    return true;
   	  }   	  
   }
   
   this.containsCoord = function(coord)
   {
   	  var bounds = new Rectangle(this.position.x, this.position.y, this.size.width, this.size.height);
   	  return bounds.contains(coord);
   }
   
   this.autoSetDimensions = function()
   {
   		if (this.animation.getPreferredSize)
   		{
   			this.size = this.animation.getPreferredSize();
   		}
   }
   
   this.autoSetDimensions();
}
