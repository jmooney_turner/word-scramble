// StaticAnimation.js

function StaticAnimation( spritesheet, frame)
{
      this.spritesheet = spritesheet;
      this.frame = frame;
      
     this.paint =  function(camera, dest)
     {
        var bounds = this.spritesheet.getBounds(this.frame);
        camera.paint(this.spritesheet.getImage(), bounds, dest);
     }
     
     this.getPreferredSize = function()
     {
     	var bounds = this.spritesheet.getBounds(this.frame);
     	return {width : bounds.width, height : bounds.height};
     }
}