//CameraFollow.js

Follow = function(source, destination)
{
	this.source = source;
	this.destination = destination;
	
	this.lateUpdate = function(elapsed)
	{
		this.destination.setCenter(this.source.getBounds().getCenter());
	}
}

Follow.prototype.toString = function()
{
	return "Follow";
}

