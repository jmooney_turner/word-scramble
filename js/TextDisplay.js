// TextDisplay.js

function TextDisplay(text, font, color)
{
      this.text = text;
      this.font = font;
      this.color = color;
      
     this.paint = function(camera, dest)
     {
        camera.paintText(this.text, new Point(dest.left, dest.bottom), this.font, this.color);
     }
}