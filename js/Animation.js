// Animation.js
function Animation( spritesheet, frames, delay)
{
      this.spritesheet = spritesheet;
      this.frames = frames;
      this.delay = delay;
      this.runtime = 0;

     this.update = function(elapsed)
     {
          this.runtime += elapsed;
     }

     this.reset = function()
     {
          this.runtime = 0;
     }

     this.paint =  function(camera, dest)
     {
        var frameIndex = Math.floor(this.runtime / this.delay);
        frameIndex = frameIndex % this.frames.length;
        var bounds = this.spritesheet.getBounds(this.frames[frameIndex]);
        camera.paint(this.spritesheet.getImage(), bounds, dest);
     }
 }
