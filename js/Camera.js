function Camera(context)
{
   this.viewBounds = new Rectangle(0, 0, context.canvas.width, context.canvas.height);
   this.worldBounds = null;
   this.context = context;
   
   this.setBounds = function(bounds)
   {
   		this.worldBounds = bounds;
   		console.log("WorldBounds " + this.worldBounds);
   }
   
   this.setCenter = function(position)
   {
   	  this.setPosition(new Point(position.x - this.viewBounds.width / 2, position.y - this.viewBounds.height / 2));
   }
   
   this.setPosition = function(position)
   {
   	  if (this.worldBounds != null)
   	  {
   	  	var x = Math.max(position.x, this.worldBounds.left);
   	  	x = Math.min(x, this.worldBounds.right - this.viewBounds.width);
   	  	var y = Math.max(position.y, this.worldBounds.top);
   	  	y = Math.min(y, this.worldBounds.bottom - this.viewBounds.height);
   	  	this.viewBounds = new Rectangle(x, y, this.viewBounds.width, this.viewBounds.height);
   	  }
   	  else
   	  {
   	  	this.viewBounds = new Rectangle(position.x, position.y, this.viewBounds.width, this.viewBounds.height);
   	  }
   }
   
   this.clear = function()
   {
   	 this.context.clearRect(0, 0, this.context.canvas.width, this.context.canvas.height);
   }
   
   this.paint = function(image, srcRect, destRect)
   {
   	  if (!Rectangle.intersects(this.viewBounds, destRect)) 
   	  {
   	  	return;
   	  }
   	  if (this.viewBounds.containsFully(destRect))
   	  {
   	  	context.drawImage(image, srcRect.left, srcRect.top, srcRect.width, srcRect.height,
            destRect.left - this.viewBounds.left, destRect.top - this.viewBounds.top, destRect.width, destRect.height); 
   	  }
   	  else
   	  {
   	  	var leftPercent = 0;
   	  	if (this.viewBounds.left > destRect.left)
   	  	{
   	  		leftPercent = (this.viewBounds.left - destRect.left) / destRect.width;
   	  	}
   	  	var rightPercent = 0;
   	  	if (this.viewBounds.right < destRect.right)
   	  	{
   	  		rightPercent = (destRect.right - this.viewBounds.right) / destRect.width;
   	  	}
   	  	
   	  	var topPercent = 0;
   	  	if (this.viewBounds.top > destRect.top)
   	  	{
   	  		topPercent = (this.viewBounds.top - destRect.top) / destRect.height;
   	  	}
   	  	var bottomPercent = 0;
   	  	if (this.viewBounds.bottom < destRect.bottom)
   	  	{
   	  		bottomPercent = (destRect.bottom - this.viewBounds.bottom) / destRect.height;
   	  	}
   	  	
   	  	var srcLeft = Math.floor(srcRect.left + srcRect.width * leftPercent);
   	  	var srcRight = Math.floor(srcRect.right - srcRect.width * rightPercent);
   	    var srcTop = Math.floor(srcRect.top + srcRect.height * topPercent);
   	  	var srcBottom= Math.floor(srcRect.bottom - srcRect.height * bottomPercent);
   	  	
   	  	var destLeft = Math.floor(destRect.left + destRect.width * leftPercent);
   	  	var destRight = Math.floor(destRect.right - destRect.width * rightPercent);
   	    var destTop = Math.floor(destRect.top + destRect.height * topPercent);
   	  	var destBottom= Math.floor(destRect.bottom - destRect.height * bottomPercent); 
   	  	
   	  	srcRect = new Rectangle(srcLeft, srcTop, srcRight - srcLeft, srcBottom - srcTop);
   	  	destRect = new Rectangle(destLeft, destTop, destRight - destLeft, destBottom - destTop);
   	  	context.drawImage(image, srcRect.left, srcRect.top, srcRect.width, srcRect.height,
            destRect.left - this.viewBounds.left, destRect.top - this.viewBounds.top, destRect.width, destRect.height); 	  	  	
   	  }
   }
   
   this.paintText = function(text, location, font, color)
   {
   	  // todo handle offscreen or partially offscreen text elements
   	  var oldColor = this.context.fillStyle;
   	  this.context.fillStyle = color;
   	  this.context.font = font;
   	  this.context.fillText(text, location.x - this.viewBounds.left, location.y - this.viewBounds.top);
      this.context.fillStyle = oldColor;
   }
   
   this.paintRect = function(rect, color)
   {
   	  var oldColor = this.context.fillStyle;
   	  this.context.fillStyle = color;
      this.context.fillRect(rect.left - this.viewBounds.left, rect.top - this.viewBounds.left,
      	rect.width, rect.height);
      this.context.fillStyle = oldColor;
   }
   
   this.convertToWorldCoord = function(viewCoords)
   {
   	  return new Point(viewCoords.x + this.viewBounds.left, viewCoords.y + this.viewBounds.top);
   }
   
   this.convertToViewCoord = function(worldCoords)
   {
   	  return new Point(worldCoords.x - this.viewBounds.left, worldCoords.y - this.viewBounds.top);
   }
}
