// SpriteSheet.js

function SpriteSheet(img, frameWid, frameHgt, numOfFrames)
     {
          this.image = img;
          this.width = frameWid;
          this.height = frameHgt;
          this.columns = Math.floor(img.width / frameWid);
          this.frame_quantity = numOfFrames;


     this.getNumberOfFrames = function()
     {
          return this.frame_quantity;
     }

     this.getImage = function()
     {
          return this.image;
     }

     this.getBounds = function(index)
     {
          return new Rectangle(
               index % this.columns * this.width,
               Math.floor(index / this.columns) * this.height,
               this.width,
               this.height);
     }

}

