// CharacterMove.js

CharacterMove = function(speed)
{
	this.speed = speed;
	this.destination = null;
	this.sprite = null;
	this.angle = 0;

	this.displayType=function(){return "CharacterMove";};
	
    this.moveTo = function(location)
    {
    	this.destination = location;
    	
    	if(this.sprite)
    	{
    		this.calculateAngle();
    	}
    }
    
    this.init = function(sprite)
    {
    	this.sprite = sprite;
    	if(this.destination)
    	{
    		this.calculateAngle();
    	}
    }
    
    this.calculateAngle = function()
    {
    	this.angle = Point.angle(this.sprite.position, this.destination);
    }
    
    this.update = function(elapsed)
    {
    	var distanceTraveled = this.speed * elapsed;
    	var distanceToTravelSqred = Point.distanceSqred(this.sprite.position, this.destination);
    	
    	if(distanceToTravelSqred < (distanceTraveled * distanceTraveled))
    	{
    		this.sprite.setPosition(this.destination.x, this.destination.y);
    	}
    	else
    	{
			this.sprite.moveX(distanceTraveled*Math.cos(this.angle));
			this.sprite.moveY(distanceTraveled*Math.sin(this.angle));    		
    	}
    }
    
    this.isMoving = function()
    {
    	if (this.sprite == null || this.destination == null)
    	{
    		return false;
    	}
    	else
    	{
    		return Point.distanceSqred(this.sprite.position, this.destination) != 0;
    	}
    }
}

CharacterMove.prototype.toString = function()
{
	return "CharacterMove";
}
