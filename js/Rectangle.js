function Rectangle(left, top, width, height)
{
	this.top = top;
	this.left = left;
	this.height = height;
	this.width = width;
	this.bottom = top + height;
	this.right = left + width;
	
	this.containsFully = function(otherRect)
	{
		return otherRect.left >= this.left &&
		       otherRect.top >= this.top && 
		       otherRect.right <= this.right &&
		       otherRect.bottom <= this.bottom;
	}
	
	this.contains = function(point)
	{
		return point.x >= this.left && point.x <= this.right &&
		       point.y >= this.top && point.y <= this.bottom;
	}
	
	this.getCenter = function()
	{
		return new Point(this.left + this.width / 2, this.top + this.height / 2);
	}
}

Rectangle.prototype.toString = function()
{
	return "[left=" + this.left + "; right=" + this.right + "; width=" + this.width +
	 "; top=" + this.top + "; bottom=" + this.bottom + "; height=" + this.height + "]";
}

Rectangle.intersects = function(r1, r2)
{
	return !(r2.left > r1.right || 
            r2.right < r1.left || 
            r2.top > r1.bottom ||
            r2.bottom < r1.top);	
}
