// ClickToMove.js
/**
 * 
 * @param {CharacterMovve} move - The object that is movine when the mouse ise clicked
 */
ClickToMove = function(move)
{
//	if (move || move.displayType()!="CharaterMove")
//	{
//		return;
//	}
	
   this.move = move;
   this.onClick = function(coord)
   {
   	var size = this.move.sprite.size;
   	this.move.moveTo(new Point(coord.x - size.width /2, coord.y - size.height / 2));
   }
}