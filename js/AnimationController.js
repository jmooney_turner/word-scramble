// AnimationController.js

AnimationController = function(animUp, animDown, animLeft, animRight, animUpIdle, animDownIdle, animLeftIdle, animRightIdle, move)
{
	this.up = animUp;
	this.down = animDown;
	this.left = animLeft;
	this.right = animRight;
	this.upIdle = animUpIdle;
	this.downIdle = animDownIdle;
	this.leftIdle = animLeftIdle;
	this.rightIdle = animRightIdle;
	this.move = move;
	this.sprite = null;
	
	this.lateUpdate = function(elapsed)
	{
		var angle = move.angle * 180 / Math.PI;
		var anim = null;
		var moving = move.isMoving();
		if((angle >= 315 && angle < 0) || (angle >= 0 && angle < 45))
		{
			anim = moving ? this.right : this.rightIdle;
		}
		else if((angle >= 45) && (angle < 135))
		{
			anim = moving ? this.down : this.downIdle;
		}
		else if((angle >= 135) && (angle < 225))
		{
			anim = moving ? this.left : this.leftIdle;
		}
		else
		{
			anim = moving ? this.up : this.upIdle;
		}
		
		this.sprite.setAnimation(anim);
	}
	
	this.init = function(sprite)
	{
		this.sprite = sprite;
	}
}
